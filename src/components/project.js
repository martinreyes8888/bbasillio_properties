import React from "react";
import 'bootstrap/dist/css/bootstrap.css';
import 'w3-css/w3.css';
import acacia1 from '../images/projects/rosavillasverdesphase2/acacia/acacia1.jpg';
import molave from '../images/projects/rosavillasverdesphase2/molave/Molave.jpeg';
import narra from '../images/projects/rosavillasverdesphase2/narra/Narra.jpeg';

function Projects() {
    return (
        <div className="Projects">
        <div class="w3-container w3-padding-32" id="projects">
            <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">Projects</h3>
        </div>
         <div class="container">
         <div class="w3-row-padding ">
           <div class="w3-col l4 m5 w3-margin-bottom">
             <div class="w3-display-container">
                 <div class="w3-display-topleft w3-black w3-padding" >Acacia</div>
                 <img id="myImg1" src={acacia1} alt="acacia1" class="img-thumbnail"/>
             </div>
         </div>
         <div class="w3-col l4 m5 w3-margin-bottom">
           <div class="w3-display-container">
               <div class="w3-display-topleft w3-black w3-padding">Molave</div>
               <img id="myImg2" src={molave} alt="Molave" class="img-thumbnail"/>
           </div>
         </div>
         <div class="w3-col l4 m5 w3-margin-bottom">
           <div class="w3-display-container">
               <div class="w3-display-topleft w3-black w3-padding">Narra</div>
               <img id="myImg3" src={narra} alt="Narra" class="img-thumbnail"/>
           </div>
         </div>
       </div>
     </div> 
     </div>  
    );
}
export default Projects;
